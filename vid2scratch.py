import cv2
import sys
import time
import numpy as np

if len(sys.argv) != 2:
    print("Usage: vid2scratch.py <video-file>")
    sys.exit(1)  # sys.argv.append(r"C:\Users\Gwitr\Desktop\scratchvid\example_vids\crysis2techdemo2.mp4")

vidcap = cv2.VideoCapture(sys.argv[1])

COLOR_THRES           = 50
QUALITY_DEGRADE_THRES = 3000  # Lines / frame
DEGRADE_MULT          = 1.4   # Decrasing will slow down the process *a lot*

Color   = []
LineLen = []
LineY   = []

def chex(c):
    return bhex(c[0] // 16, 1) + bhex(c[1] // 16, 1) + bhex(c[2] // 16, 1)
def bhex(n, l=2):
    return hex(n)[2:].zfill(l)

def decodelines(color, liney, linelen):
    image = np.zeros([120, 90, 3]).astype(np.uint8)
    lasty = -1
    x = 0
    for beggining_color, y, clen in zip(color, liney, linelen):
        if lasty > y:
            yield image
            image = np.zeros([120, 90, 3]).astype(np.uint8)
            x = 0
        for i in range(x, x+clen):
            i = min(i, 119)
            image[i, y] = beggining_color
        #cv2.imshow("image", np.rot90(image, axes=[0, 1]))
        #if cv2.waitKey(1000000) == 27:
        #    sys.exit(1)
        lasty = y
        x += clen + 1
        if x >= 120:
            x = 0

def convert_image(image):
    global Color, LineLen, LineY
    def _conv(image, COLOR_THRES):
        Color = []
        LineLen = []
        LineY = []

        lcount = 0

        for y, row in enumerate(image):
            # Reset variables
            beggining_color = None
            clen = 0
            for color in row:
                if beggining_color is None:
                    beggining_color = color
                # If the average difference between this color and beggining is smaller than COLOR_THRES, then...
                if np.sum(np.abs(color - beggining_color)) / 3 < COLOR_THRES:
                    # Increment line length.
                    clen += 1
                else:  # Else:
                    # Add line to list
                    Color.append(list(beggining_color))
                    LineLen.append(clen)
                    LineY.append(y)
                    # Reset variables again
                    clen = 0
                    beggining_color = None
                    
                    lcount += 1
            # Add last line
            Color.append(list(color))
            LineLen.append(clen)
            LineY.append(y)
            lcount += 1
        return lcount, Color, LineLen, LineY
    my_ct = COLOR_THRES
    res, colors, llen, ly = _conv(image, my_ct)
    while len(llen) > QUALITY_DEGRADE_THRES:
        my_ct *= DEGRADE_MULT
        my_ct = int(my_ct)
        res, colors, llen, ly = _conv(image, my_ct)
    Color += colors
    LineLen += llen
    LineY += ly
    return res, my_ct

success,image = vidcap.read()
success = True
count = 0
actual_start = time.perf_counter()
while success:
    try:
        success,image = vidcap.read()
        if image.shape[0] != 90:
            print("Video size must be 120x90")
            sys.exit(1)
        if success:
            clen = len(Color)
            st = time.perf_counter()
            L, my_ct = convert_image(image)
            """try:
                speed = (time.perf_counter()-actual_start)/(count/5)
            except ZeroDivisionError:
                speed = float("nan")"""  # Broken / wrong
            timestamp = "%s:%s.%s" % (
                str(int((count/5)//60)).zfill(2),
                str(int((count/5)%60)).zfill(2),
                str(int(count*200%1000)).zfill(3)
            )
            print("COLOR_THRES: %3d | Frame: %3d | Lines: %5d | Frame time: %4d ms | Timestamp: %s" % (my_ct, count, L, (time.perf_counter()-st)*1000, timestamp), end="\r")

        count += 1
    except KeyboardInterrupt:
        print("Moving on...")
        success = False

time.sleep(1)

i = 0
decoder = decodelines(Color, LineY, LineLen)
for image in decoder:
    cv2.imshow("image", np.flip(np.rot90(image, axes=[1, 0]), 1))
    if cv2.waitKey(int(1000/5)) == 27:
        cv2.destroyAllWindows()
        break

print("Writing to file... [Ctrl+C to cancel]")
try:
    time.sleep(1)
except KeyboardInterrupt:
    print("Interrupted by user.")
    sys.exit(1)

with open("Color.txt", "w") as f:
    f.write("\n".join([chex(i) for i in Color]))
print("Wrote Color.txt")

with open("LineY.txt", "w") as f:
    f.write("\n".join([bhex(i) for i in LineY]))
print("Wrote LineY.txt")

with open("LineLen.txt", "w") as f:
    f.write("\n".join([bhex(i) for i in LineLen]))
print("Wrote LineLen.txt")
